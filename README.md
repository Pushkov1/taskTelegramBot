# 
# Repository with testing task
<h3>Приложение сосотоит из трех модулей:</h3>
<ul>
<li>backend</li>
<li>frontend</li>
<li>telegramBot</li>
</ul>
<hr>
<p>Backend реализован как REST приложение, но сохранена возможность 
использование MVC шаблона.</p>
<p>
В качестве интерфейса используется одностраничное приложение на VueJS.
</p>
<p>
В проекте использовались технологии:
</p>
<p>
SpringBoot, SpringDAtaJpa, TheMyLeaf, Lombok, VueJS, BootstrapVue, Vuex, SpringTelegramBot.
</p>
<p>
В качестве базы данных использовалась embedded H2
</p>